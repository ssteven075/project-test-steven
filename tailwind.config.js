/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      container: {
        center: true,
        padding: "1.5rem",
        screens: {
          "2xl": "1360px",
        },
      },
      future: {
        hoverOnlyWhenSupported: true,
      },
      height: {
        screen: ["100vh", "100dvh"],
      },
    },
  },
  plugins: [],
};
