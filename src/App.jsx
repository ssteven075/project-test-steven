import { useEffect, useState } from "react";
import Bg from "./assets/bg.jpg";
import AboutBg from "./assets/about-bg.jpg";
import {
  LucideChevronLeftCircle,
  LucideChevronRightCircle,
  LucideLandmark,
  LucideLightbulb,
  LucideMenu,
  LucideScale,
} from "lucide-react";
import { AnimatePresence, motion } from "framer-motion";
import useMeasure from "react-use-measure";

let variants = {
  enter: ({ direction, width }) => ({ x: direction * width }),
  center: { x: 0 },
  exit: ({ direction, width }) => ({ x: direction * -width }),
};

function App() {
  const [active, setActive] = useState(0);
  const [clicked, setClicked] = useState("");
  const [disabled, setDisabled] = useState(false);
  let direction = clicked === "right" ? 1 : -1;
  let [ref, { width }] = useMeasure();

  useEffect(() => {
    setDisabled(true);
    const timeout = setTimeout(() => {
      setDisabled(false);
    }, 500);

    return () => clearTimeout(timeout);
  }, [active]);

  const [open, setOpen] = useState(false);

  return (
    <main>
      {/* navbar */}
      <nav className="bg-white w-full relative">
        <div className="w-full absolute left-0 top-[62px] z-10">
          <motion.ul
            initial={false}
            animate={{ height: open ? 280 : 0, transition: { type: "tween", duration: 0.3 } }}
            className="flex flex-col h-full bg-white overflow-hidden"
          >
            <li>
              <a href="#" className="hover:bg-black/10 p-4 block active:bg-black/10 duration-200">
                HISTORY
              </a>
            </li>
            <li>
              <a href="#" className="hover:bg-black/10 p-4 block active:bg-black/10 duration-200">
                VISION MISSION
              </a>
            </li>
            <li>
              <a href="#" className="hover:bg-black/10 p-4 block active:bg-black/10 duration-200">
                OUR WORK
              </a>
            </li>
            <li>
              <a href="#" className="hover:bg-black/10 p-4 block active:bg-black/10 duration-200">
                OUR TEAM
              </a>
            </li>
            <li>
              <a href="#" className="hover:bg-black/10 p-4 block active:bg-black/10 duration-200">
                CONTACT
              </a>
            </li>
          </motion.ul>
        </div>

        <div className="container mx-auto flex justify-between items-center">
          <a href="#" className="font-bold text-2xl py-4">
            Company
          </a>

          <div className="hidden max-sm:block">
            <button onClick={() => setOpen(!open)} className="active:bg-black/10 duration-200 p-2 rounded">
              <LucideMenu />
            </button>
          </div>

          <ul className="flex max-sm:hidden">
            <div className="relative group">
              <p className="peer font-medium cursor-pointer py-4 px-6 hover:bg-black/10">ABOUT</p>
              <div className="group-hover:opacity-100 peer-hover:opacity-100 opacity-0 absolute bg-white flex flex-col z-10 top-14 w-44">
                <li>
                  <a href="#" className="font-medium hover:bg-zinc-800 hover:text-white w-full py-3 pl-6 pr-3 block">
                    HISTORY
                  </a>
                </li>
                <li>
                  <a href="#" className="font-medium hover:bg-zinc-800 hover:text-white w-full py-3 pl-6 pr-3 block">
                    VISION MISSION
                  </a>
                </li>
              </div>
            </div>

            <li>
              <a href="#" className="font-medium py-4 px-6 hover:bg-black/10 block">
                OUR WORK
              </a>
            </li>
            <li>
              <a href="#" className="font-medium py-4 px-6 hover:bg-black/10 block">
                OUR TEAM
              </a>
            </li>
            <li>
              <a href="#" className="font-medium py-4 px-6 hover:bg-black/10 block">
                CONTACT
              </a>
            </li>
          </ul>
        </div>
      </nav>

      {/* carousel */}
      <div className="w-screen relative">
        <div ref={ref} className="relative flex h-screen sm:max-h-[90vh] w-full">
          <AnimatePresence custom={{ direction, width }}>
            <motion.img
              key={active}
              variants={variants}
              initial="enter"
              animate="center"
              exit="exit"
              custom={{ direction, width }}
              transition={{ type: "tween", duration: 0.5 }}
              src={active === 0 ? Bg : AboutBg}
              alt="Background"
              className="w-full h-screen sm:max-h-[90vh] object-cover absolute"
            />
          </AnimatePresence>

          <div className="absolute max-sm:w-full max-w-3xl p-4 bg-black/60 bottom-[12%] left-1/4 -translate-x-1/4">
            <p className="text-white uppercase font-bold text-3xl">
              {active === 0
                ? "This is a place where technology & creativity fused into digital chemistry"
                : "We don't have the best but we have the greatest team"}
            </p>
          </div>

          <div className="absolute left-1/2 -translate-x-1/2 bottom-8">
            <div className="flex gap-2">
              <div className={`w-3 h-3 rounded-full duration-200 ${active === 0 ? "bg-white" : "bg-white/40"}`}></div>
              <div className={`w-3 h-3 rounded-full duration-200 ${active === 1 ? "bg-white" : "bg-white/40"}`}></div>
            </div>
          </div>
        </div>
        <button
          disabled={disabled}
          onClick={() => {
            setActive(() => {
              setClicked("left");
              if (active === 0) {
                return 1;
              } else {
                return 0;
              }
            });
          }}
          className="absolute top-1/2 -translate-y-1/2 left-4 active:opacity-80 duration-200"
        >
          <LucideChevronLeftCircle className="text-white w-8 h-8" />
        </button>
        <button
          disabled={disabled}
          onClick={() => {
            setActive(() => {
              setClicked("right");
              if (active === 0) {
                return 1;
              } else {
                return 0;
              }
            });
          }}
          className="absolute top-1/2 -translate-y-1/2 right-4 active:opacity-80 duration-200"
        >
          <LucideChevronRightCircle className="text-white w-8 h-8" />
        </button>
      </div>

      {/* our values */}
      <div className="container py-10 md:py-20">
        <h2 className="text-3xl font-bold text-center">OUR VALUES</h2>
        <div className="grid md:grid-cols-2 xl:grid-cols-3 w-fit gap-8 mx-auto mt-10">
          <div className="relative px-6 py-8 bg-[#ea7c6f] text-white w-80 flex flex-col items-center">
            <LucideLightbulb />
            <p className="font-bold text-xl mt-2">INNOVATIVE</p>
            <p className="mt-4 text-center">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit cum fugit, exercitationem abit.
            </p>

            <div className="absolute max-md:hidden -right-4 top-1/2 -translate-y-1/2 w-8 h-8 bg-[#de5145] rotate-45 -z-10"></div>
          </div>

          <div className="relative px-6 py-8 bg-[#6a996f] text-white w-80 flex flex-col items-center">
            <LucideLandmark />
            <p className="font-bold text-xl mt-2">LOYALTY</p>
            <p className="mt-4 text-center">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit cum fugit, exercitationem abit.
            </p>

            <div className="absolute max-md:hidden -right-4 top-1/2 -translate-y-1/2 w-8 h-8 bg-[#547b56] rotate-45 -z-10"></div>
          </div>

          <div className="px-6 py-8 bg-[#5696c2] text-white w-80 flex flex-col items-center">
            <LucideScale />
            <p className="font-bold text-xl mt-2">RESPECT</p>
            <p className="mt-4 text-center">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit cum fugit, exercitationem abit.
            </p>
          </div>
        </div>
      </div>

      <div className="container py-10 md:py-20">
        {/* contact us */}
        <h2 className="text-3xl font-bold text-center">CONTACT US</h2>
        <form onSubmit={(e) => e.preventDefault()} className="mx-auto max-w-xl space-y-4 mt-10">
          <div className="flex flex-col gap-2">
            <label htmlFor="nama" className="font-semibold">
              Name
            </label>
            <input
              required
              type="text"
              id="name"
              name="name"
              placeholder="Your name..."
              className="px-3 py-2 border shadow"
            />
          </div>

          <div className="flex flex-col gap-2">
            <label htmlFor="email" className="font-semibold">
              Email
            </label>
            <input
              required
              type="text"
              id="email"
              name="email"
              placeholder="Your email..."
              className="px-3 py-2 border shadow"
            />
          </div>

          <div className="flex flex-col gap-2">
            <label htmlFor="message" className="font-semibold">
              Message
            </label>
            <textarea
              required
              type="text"
              id="message"
              name="message"
              placeholder="Your message..."
              className="px-3 py-2 border shadow"
            />
          </div>
          <button type="submit" className="bg-[#5696c2] w-full py-2 px-3 text-white active:opacity-80 duration-200">
            SUBMIT
          </button>
        </form>
      </div>
    </main>
  );
}

export default App;
